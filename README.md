vim IDE
=======


### language support

* shell
* markdown/html/css/js
* python auto-complete
* golang auto-complete
* golang go to decleration 


### short-cut

* go to decleration: `ctrl+]`
* backward: `ctrl+o`
* forward: `ctrl+i`
* file nevigate: `,+n`
* outline: `,+tl`

### install
```shell
./install.sh
```

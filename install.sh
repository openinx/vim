#!/bin/bash

if [ "$GOPATH" == "" ];then
    echo "GOPATH is empty, please set GOPATH."
    exit
fi

# check current directory is ok
CUR_DIR=`pwd`
if [ ! -r $CUR_DIR/_vim ]; then
    echo "current directory has no _vim sub dir, please change dir to parent of vim"
    exit
fi

if [ ! -r $CUR_DIR/_vimrc ]; then
    echo "current directory has no _vimrc sub dir, please change dir to parent of vimrc"
    exit
fi


#
# create software link to $HOME/.vim and $HOME/.vimrc
#
ln -nfs $CUR_DIR/_vim $HOME/.vim
ln -nfs $CUR_DIR/_vimrc $HOME/.vimrc

# this is array
# full with git repositories name
GOLANG_ARRAY=(tools text net sys image goimports exp crypto)

#git clone golang.org/x/* package
for SUB_PATH in ${GOLANG_ARRAY[*]}; do
    CLONE_PATH=$GOPATH/src/golang.org/x/$SUB_PATH
    echo $CLONE_PATH
    if [ -r $CLONE_PATH ]; then
        continue
    fi
    REP=git@github.com:golang/$SUB_PATH.git
    git clone $REP $CLONE_PATH
done

